'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FieldBody = exports.FieldLabel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var fieldClases = {
  addons: 'has-addons',
  addonsCentered: 'has-addons has-addons-centered',
  addonsRight: 'has-addons has-addons-right',
  grouped: 'is-grouped',
  groupedCentered: 'is-grouped is-grouped-centered',
  groupedRight: 'is-grouped is-grouped-right',
  horizontal: 'is-horizontal'
};

var Field = function (_BulmaAnimatedCompone) {
  _inherits(Field, _BulmaAnimatedCompone);

  function Field() {
    _classCallCheck(this, Field);

    return _possibleConstructorReturn(this, (Field.__proto__ || Object.getPrototypeOf(Field)).apply(this, arguments));
  }

  _createClass(Field, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'fieldClases',
    get: function get() {
      var _this2 = this;

      return this.fieldProps.filter(function (prop) {
        return _this2.props[prop];
      }).map(function (prop) {
        return fieldClases[prop];
      }).join(' ');
    }
  }, {
    key: 'fieldProps',
    get: function get() {
      return Object.keys(fieldClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(Field.prototype.__proto__ || Object.getPrototypeOf(Field.prototype), 'extendedProps', this)), _toConsumableArray(this.fieldProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'field ' + _get(Field.prototype.__proto__ || Object.getPrototypeOf(Field.prototype), 'className', this) + ' ' + this.fieldClases;
    }
  }]);

  return Field;
}(_BulmaAnimatedComponent2.default);

exports.default = Field;


Field.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  addons: false,
  addonsCentered: false,
  addonsRight: false,
  grouped: false,
  groupedCentered: false,
  groupedRight: false,
  horizontal: false
});

var FieldLabel = exports.FieldLabel = function (_BulmaAnimatedCompone2) {
  _inherits(FieldLabel, _BulmaAnimatedCompone2);

  function FieldLabel() {
    _classCallCheck(this, FieldLabel);

    return _possibleConstructorReturn(this, (FieldLabel.__proto__ || Object.getPrototypeOf(FieldLabel)).apply(this, arguments));
  }

  _createClass(FieldLabel, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'field-label ' + _get(FieldLabel.prototype.__proto__ || Object.getPrototypeOf(FieldLabel.prototype), 'className', this);
    }
  }]);

  return FieldLabel;
}(_BulmaAnimatedComponent2.default);

var FieldBody = exports.FieldBody = function (_FieldLabel) {
  _inherits(FieldBody, _FieldLabel);

  function FieldBody() {
    _classCallCheck(this, FieldBody);

    return _possibleConstructorReturn(this, (FieldBody.__proto__ || Object.getPrototypeOf(FieldBody)).apply(this, arguments));
  }

  _createClass(FieldBody, [{
    key: 'className',
    get: function get() {
      return 'field-body ' + _get(FieldBody.prototype.__proto__ || Object.getPrototypeOf(FieldBody.prototype), 'className', this);
    }
  }]);

  return FieldBody;
}(FieldLabel);