'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var imageClases = {
  'is16x16': 'is-16x16',
  'is32x32': 'is-32x32',
  'is48x48': 'is-48x48',
  'is64x64': 'is-64x64',
  'is96x96': 'is-96x96',
  'is128x96': 'is-128x128',
  'is1by1': 'is-1by1',
  'is4by3': 'is-4by3',
  'is3by2': 'is-3by2',
  'is16by9': 'is-16by9',
  'is2by1': 'is-2by1'
};

var Image = function (_BulmaAnimatedCompone) {
  _inherits(Image, _BulmaAnimatedCompone);

  function Image() {
    _classCallCheck(this, Image);

    return _possibleConstructorReturn(this, (Image.__proto__ || Object.getPrototypeOf(Image)).apply(this, arguments));
  }

  _createClass(Image, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'figure',
        { className: this.className, style: this.style },
        _react2.default.createElement('img', this.nativeProps)
      );
    }
  }, {
    key: 'imageClases',
    get: function get() {
      var _this2 = this;

      return this.imageProps.filter(function (prop) {
        return _this2.props[prop];
      }).map(function (prop) {
        return imageClases[prop];
      }).join(' ');
    }
  }, {
    key: 'imageProps',
    get: function get() {
      return Object.keys(imageClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(Image.prototype.__proto__ || Object.getPrototypeOf(Image.prototype), 'extendedProps', this)), _toConsumableArray(this.imageProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'image ' + _get(Image.prototype.__proto__ || Object.getPrototypeOf(Image.prototype), 'className', this) + ' ' + this.imageClases;
    }
  }]);

  return Image;
}(_BulmaAnimatedComponent2.default);

exports.default = Image;


Image.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  'is16x16': false,
  'is32x32': false,
  'is48x48': false,
  'is64x64': false,
  'is96x96': false,
  'is128x96': false,
  'is1by1': false,
  'is4by3': false,
  'is3by2': false,
  'is16by9': false,
  'is2by1': false
});