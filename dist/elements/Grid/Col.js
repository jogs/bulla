'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Column = exports.Columns = undefined;

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Grid2 = require('./Grid');

var _Grid3 = _interopRequireDefault(_Grid2);

var _BulmaComponent2 = require('../../core/BulmaComponent');

var _BulmaComponent3 = _interopRequireDefault(_BulmaComponent2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var columnClases = {
  desktop: 'is-desktop',
  tablet: 'is-tablet',
  mobile: 'is-mobile',
  multiline: 'is-multiline',
  narrow: 'is-narrow',
  gapless: 'is-gapless'
};

var Columns = exports.Columns = function (_BulmaComponent) {
  _inherits(Columns, _BulmaComponent);

  function Columns() {
    _classCallCheck(this, Columns);

    return _possibleConstructorReturn(this, (Columns.__proto__ || Object.getPrototypeOf(Columns)).apply(this, arguments));
  }

  _createClass(Columns, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className + ' columns' }),
        this.props.children
      );
    }
  }]);

  return Columns;
}(_BulmaComponent3.default);

var Column = exports.Column = function (_Grid) {
  _inherits(Column, _Grid);

  function Column() {
    _classCallCheck(this, Column);

    return _possibleConstructorReturn(this, (Column.__proto__ || Object.getPrototypeOf(Column)).apply(this, arguments));
  }

  _createClass(Column, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className + ' column' }),
        this.props.children
      );
    }
  }, {
    key: '_bulmaClases',
    get: function get() {
      return Object.assign({}, _get(Column.prototype.__proto__ || Object.getPrototypeOf(Column.prototype), '_bulmaClases', this), columnClases);
    }
  }]);

  return Column;
}(_Grid3.default);