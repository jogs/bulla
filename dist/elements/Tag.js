'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var tagClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  danger: 'is-danger',
  white: 'is-white',
  light: 'is-light',
  dark: 'is-dark',
  black: 'is-black',
  link: 'is-link',
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large'
};

var Tag = function (_BulmaAnimatedCompone) {
  _inherits(Tag, _BulmaAnimatedCompone);

  function Tag() {
    _classCallCheck(this, Tag);

    return _possibleConstructorReturn(this, (Tag.__proto__ || Object.getPrototypeOf(Tag)).apply(this, arguments));
  }

  _createClass(Tag, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'span',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'tagClases',
    get: function get() {
      var _this2 = this;

      return this.tagProps.filter(function (prop) {
        return _this2.props[prop];
      }).map(function (prop) {
        return tagClases[prop];
      }).join(' ');
    }
  }, {
    key: 'tagProps',
    get: function get() {
      return Object.keys(tagClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(Tag.prototype.__proto__ || Object.getPrototypeOf(Tag.prototype), 'extendedProps', this)), _toConsumableArray(this.tagProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'tag ' + _get(Tag.prototype.__proto__ || Object.getPrototypeOf(Tag.prototype), 'className', this) + ' ' + this.tagClases;
    }
  }]);

  return Tag;
}(_BulmaAnimatedComponent2.default);

exports.default = Tag;


Tag.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  danger: false,
  white: false,
  light: false,
  dark: false,
  black: false,
  link: false,
  small: false,
  medium: false,
  large: false
});