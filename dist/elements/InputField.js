'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var inputClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  danger: 'is-danger',
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large',
  focused: 'is-focused',
  hovered: 'is-hovered',
  loading: 'is-loading'
};

var InputField = function (_BulmaAnimatedCompone) {
  _inherits(InputField, _BulmaAnimatedCompone);

  function InputField() {
    _classCallCheck(this, InputField);

    return _possibleConstructorReturn(this, (InputField.__proto__ || Object.getPrototypeOf(InputField)).apply(this, arguments));
  }

  _createClass(InputField, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement('input', _extends({}, this.nativeProps, { className: this.className, style: this.style }));
    }
  }, {
    key: 'inputClases',
    get: function get() {
      var _this2 = this;

      return this.inputProps.filter(function (prop) {
        return _this2.props[prop];
      }).map(function (prop) {
        return inputClases[prop];
      }).join(' ');
    }
  }, {
    key: 'inputProps',
    get: function get() {
      return Object.keys(inputClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(InputField.prototype.__proto__ || Object.getPrototypeOf(InputField.prototype), 'extendedProps', this)), _toConsumableArray(this.inputProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'input ' + _get(InputField.prototype.__proto__ || Object.getPrototypeOf(InputField.prototype), 'className', this) + ' ' + this.inputClases;
    }
  }]);

  return InputField;
}(_BulmaAnimatedComponent2.default);

exports.default = InputField;


InputField.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  danger: false,
  small: false,
  medium: false,
  large: false,
  focused: false,
  hovered: false,
  loading: false
});