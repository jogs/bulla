'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MenuLabel = exports.MenuList = exports.Menu = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Menu = function (_BulmaAnimatedCompone) {
  _inherits(Menu, _BulmaAnimatedCompone);

  function Menu() {
    _classCallCheck(this, Menu);

    return _possibleConstructorReturn(this, (Menu.__proto__ || Object.getPrototypeOf(Menu)).apply(this, arguments));
  }

  _createClass(Menu, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'aside',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'menu ' + _get(Menu.prototype.__proto__ || Object.getPrototypeOf(Menu.prototype), 'className', this);
    }
  }]);

  return Menu;
}(_BulmaAnimatedComponent2.default);

var MenuList = function (_BulmaAnimatedCompone2) {
  _inherits(MenuList, _BulmaAnimatedCompone2);

  function MenuList() {
    _classCallCheck(this, MenuList);

    return _possibleConstructorReturn(this, (MenuList.__proto__ || Object.getPrototypeOf(MenuList)).apply(this, arguments));
  }

  _createClass(MenuList, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'ul',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'menu-list ' + _get(MenuList.prototype.__proto__ || Object.getPrototypeOf(MenuList.prototype), 'className', this);
    }
  }]);

  return MenuList;
}(_BulmaAnimatedComponent2.default);

var MenuLabel = function (_BulmaAnimatedCompone3) {
  _inherits(MenuLabel, _BulmaAnimatedCompone3);

  function MenuLabel() {
    _classCallCheck(this, MenuLabel);

    return _possibleConstructorReturn(this, (MenuLabel.__proto__ || Object.getPrototypeOf(MenuLabel)).apply(this, arguments));
  }

  _createClass(MenuLabel, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'p',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'menu-label ' + _get(MenuLabel.prototype.__proto__ || Object.getPrototypeOf(MenuLabel.prototype), 'className', this);
    }
  }]);

  return MenuLabel;
}(_BulmaAnimatedComponent2.default);

exports.Menu = Menu;
exports.MenuList = MenuList;
exports.MenuLabel = MenuLabel;