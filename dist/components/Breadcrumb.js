'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BreadcrumbElement = exports.Breadcrumb = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var breadcrumbClases = {
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large',
  centered: 'is-centered',
  right: 'is-right',
  arrow: 'has-arrow-separator',
  bullet: 'has-bullet-separator',
  dot: 'has-dot-separator',
  succeeds: 'has-succeeds-separator'
};

var Breadcrumb = function (_BulmaAnimatedCompone) {
  _inherits(Breadcrumb, _BulmaAnimatedCompone);

  function Breadcrumb() {
    _classCallCheck(this, Breadcrumb);

    return _possibleConstructorReturn(this, (Breadcrumb.__proto__ || Object.getPrototypeOf(Breadcrumb)).apply(this, arguments));
  }

  _createClass(Breadcrumb, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'nav',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        _react2.default.createElement(
          'ul',
          null,
          this.props.children
        )
      );
    }
  }, {
    key: 'breadcrumbClases',
    get: function get() {
      var _this2 = this;

      return this.breadcrumbProps.filter(function (prop) {
        return _this2.props[prop];
      }).map(function (prop) {
        return breadcrumbClases[prop];
      }).join(' ');
    }
  }, {
    key: 'breadcrumbProps',
    get: function get() {
      return Object.keys(breadcrumbClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(Breadcrumb.prototype.__proto__ || Object.getPrototypeOf(Breadcrumb.prototype), 'extendedProps', this)), _toConsumableArray(this.breadcrumbProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'breadcrumb ' + _get(Breadcrumb.prototype.__proto__ || Object.getPrototypeOf(Breadcrumb.prototype), 'className', this) + ' ' + this.breadcrumbClases;
    }
  }]);

  return Breadcrumb;
}(_BulmaAnimatedComponent2.default);

var elementClases = {
  active: 'is-active'
};

var BreadcrumbElement = function (_BulmaAnimatedCompone2) {
  _inherits(BreadcrumbElement, _BulmaAnimatedCompone2);

  function BreadcrumbElement() {
    _classCallCheck(this, BreadcrumbElement);

    return _possibleConstructorReturn(this, (BreadcrumbElement.__proto__ || Object.getPrototypeOf(BreadcrumbElement)).apply(this, arguments));
  }

  _createClass(BreadcrumbElement, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'li',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'elementClases',
    get: function get() {
      var _this4 = this;

      return this.elementProps.filter(function (prop) {
        return _this4.props[prop];
      }).map(function (prop) {
        return elementClases[prop];
      }).join(' ');
    }
  }, {
    key: 'elementProps',
    get: function get() {
      return Object.keys(elementClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(BreadcrumbElement.prototype.__proto__ || Object.getPrototypeOf(BreadcrumbElement.prototype), 'extendedProps', this)), _toConsumableArray(this.elementProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return _get(BreadcrumbElement.prototype.__proto__ || Object.getPrototypeOf(BreadcrumbElement.prototype), 'className', this) + ' ' + this.elementClases;
    }
  }]);

  return BreadcrumbElement;
}(_BulmaAnimatedComponent2.default);

Breadcrumb.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  small: false,
  medium: false,
  large: false,
  centered: false,
  right: false,
  arrow: false,
  bullet: false,
  dot: false,
  succeeds: false
});

BreadcrumbElement.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  active: false
});

exports.Breadcrumb = Breadcrumb;
exports.BreadcrumbElement = BreadcrumbElement;