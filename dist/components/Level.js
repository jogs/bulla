'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LevelItem = exports.LevelLeft = exports.LevelRight = exports.Level = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Level = function (_BulmaAnimatedCompone) {
  _inherits(Level, _BulmaAnimatedCompone);

  function Level() {
    _classCallCheck(this, Level);

    return _possibleConstructorReturn(this, (Level.__proto__ || Object.getPrototypeOf(Level)).apply(this, arguments));
  }

  _createClass(Level, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'nav',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'level ' + _get(Level.prototype.__proto__ || Object.getPrototypeOf(Level.prototype), 'className', this);
    }
  }]);

  return Level;
}(_BulmaAnimatedComponent2.default);

var LevelRight = function (_BulmaAnimatedCompone2) {
  _inherits(LevelRight, _BulmaAnimatedCompone2);

  function LevelRight() {
    _classCallCheck(this, LevelRight);

    return _possibleConstructorReturn(this, (LevelRight.__proto__ || Object.getPrototypeOf(LevelRight)).apply(this, arguments));
  }

  _createClass(LevelRight, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'level-right ' + _get(LevelRight.prototype.__proto__ || Object.getPrototypeOf(LevelRight.prototype), 'className', this);
    }
  }]);

  return LevelRight;
}(_BulmaAnimatedComponent2.default);

var LevelLeft = function (_LevelRight) {
  _inherits(LevelLeft, _LevelRight);

  function LevelLeft() {
    _classCallCheck(this, LevelLeft);

    return _possibleConstructorReturn(this, (LevelLeft.__proto__ || Object.getPrototypeOf(LevelLeft)).apply(this, arguments));
  }

  _createClass(LevelLeft, [{
    key: 'className',
    get: function get() {
      return 'level-left ' + _get(LevelLeft.prototype.__proto__ || Object.getPrototypeOf(LevelLeft.prototype), 'animatedBulmaClases', this);
    }
  }]);

  return LevelLeft;
}(LevelRight);

var levelClases = {
  mobile: 'is-mobile'
};

var LevelItem = function (_BulmaAnimatedCompone3) {
  _inherits(LevelItem, _BulmaAnimatedCompone3);

  function LevelItem() {
    _classCallCheck(this, LevelItem);

    return _possibleConstructorReturn(this, (LevelItem.__proto__ || Object.getPrototypeOf(LevelItem)).apply(this, arguments));
  }

  _createClass(LevelItem, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'levelClases',
    get: function get() {
      var _this5 = this;

      return this.levelProps.filter(function (prop) {
        return _this5.props[prop];
      }).map(function (prop) {
        return levelClases[prop];
      }).join(' ');
    }
  }, {
    key: 'levelProps',
    get: function get() {
      return Object.keys(levelClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(LevelItem.prototype.__proto__ || Object.getPrototypeOf(LevelItem.prototype), 'extendedProps', this)), _toConsumableArray(this.levelProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'level-item ' + _get(LevelItem.prototype.__proto__ || Object.getPrototypeOf(LevelItem.prototype), 'className', this) + ' ' + this.levelClases;
    }
  }]);

  return LevelItem;
}(_BulmaAnimatedComponent2.default);

LevelItem.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  mobile: false
});

exports.Level = Level;
exports.LevelRight = LevelRight;
exports.LevelLeft = LevelLeft;
exports.LevelItem = LevelItem;