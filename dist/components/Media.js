'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Content = exports.MediaDeleteLeft = exports.MediaDeleteRight = exports.MediaImageLeft = exports.MediaImageRight = exports.MediaContent = exports.Media = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _elements = require('../elements');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Media = function (_BulmaAnimatedCompone) {
  _inherits(Media, _BulmaAnimatedCompone);

  function Media() {
    _classCallCheck(this, Media);

    return _possibleConstructorReturn(this, (Media.__proto__ || Object.getPrototypeOf(Media)).apply(this, arguments));
  }

  _createClass(Media, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'article',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'media ' + _get(Media.prototype.__proto__ || Object.getPrototypeOf(Media.prototype), 'className', this);
    }
  }]);

  return Media;
}(_BulmaAnimatedComponent2.default);

var MediaContent = function (_BulmaAnimatedCompone2) {
  _inherits(MediaContent, _BulmaAnimatedCompone2);

  function MediaContent() {
    _classCallCheck(this, MediaContent);

    return _possibleConstructorReturn(this, (MediaContent.__proto__ || Object.getPrototypeOf(MediaContent)).apply(this, arguments));
  }

  _createClass(MediaContent, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'media-content ' + _get(MediaContent.prototype.__proto__ || Object.getPrototypeOf(MediaContent.prototype), 'className', this);
    }
  }]);

  return MediaContent;
}(_BulmaAnimatedComponent2.default);

var MediaImageLeft = function (_Image) {
  _inherits(MediaImageLeft, _Image);

  function MediaImageLeft() {
    _classCallCheck(this, MediaImageLeft);

    return _possibleConstructorReturn(this, (MediaImageLeft.__proto__ || Object.getPrototypeOf(MediaImageLeft)).apply(this, arguments));
  }

  _createClass(MediaImageLeft, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'figure',
        { className: 'media-left' },
        _react2.default.createElement(
          'p',
          { className: this.className, style: this.style },
          _react2.default.createElement('img', this.nativeProps)
        )
      );
    }
  }]);

  return MediaImageLeft;
}(_elements.Image);

var MediaImageRight = function (_Image2) {
  _inherits(MediaImageRight, _Image2);

  function MediaImageRight() {
    _classCallCheck(this, MediaImageRight);

    return _possibleConstructorReturn(this, (MediaImageRight.__proto__ || Object.getPrototypeOf(MediaImageRight)).apply(this, arguments));
  }

  _createClass(MediaImageRight, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'figure',
        { className: 'media-right' },
        _react2.default.createElement(
          'p',
          { className: this.className, style: this.style },
          _react2.default.createElement('img', this.nativeProps)
        )
      );
    }
  }]);

  return MediaImageRight;
}(_elements.Image);

var MediaDeleteRight = function (_BulmaAnimatedCompone3) {
  _inherits(MediaDeleteRight, _BulmaAnimatedCompone3);

  function MediaDeleteRight() {
    _classCallCheck(this, MediaDeleteRight);

    return _possibleConstructorReturn(this, (MediaDeleteRight.__proto__ || Object.getPrototypeOf(MediaDeleteRight)).apply(this, arguments));
  }

  _createClass(MediaDeleteRight, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        _react2.default.createElement(_elements.Delete, null)
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'media-right ' + _get(MediaDeleteRight.prototype.__proto__ || Object.getPrototypeOf(MediaDeleteRight.prototype), 'className', this);
    }
  }]);

  return MediaDeleteRight;
}(_BulmaAnimatedComponent2.default);

var MediaDeleteLeft = function (_MediaDeleteRight) {
  _inherits(MediaDeleteLeft, _MediaDeleteRight);

  function MediaDeleteLeft() {
    _classCallCheck(this, MediaDeleteLeft);

    return _possibleConstructorReturn(this, (MediaDeleteLeft.__proto__ || Object.getPrototypeOf(MediaDeleteLeft)).apply(this, arguments));
  }

  _createClass(MediaDeleteLeft, [{
    key: 'className',
    get: function get() {
      return 'media-left ' + _get(MediaDeleteLeft.prototype.__proto__ || Object.getPrototypeOf(MediaDeleteLeft.prototype), 'animatedBulmaClasses', this);
    }
  }]);

  return MediaDeleteLeft;
}(MediaDeleteRight);

var Content = function (_MediaContent) {
  _inherits(Content, _MediaContent);

  function Content() {
    _classCallCheck(this, Content);

    return _possibleConstructorReturn(this, (Content.__proto__ || Object.getPrototypeOf(Content)).apply(this, arguments));
  }

  _createClass(Content, [{
    key: 'className',
    get: function get() {
      return 'content ' + _get(Content.prototype.__proto__ || Object.getPrototypeOf(Content.prototype), 'animatedBulmaClasses', this);
    }
  }]);

  return Content;
}(MediaContent);

exports.Media = Media;
exports.MediaContent = MediaContent;
exports.MediaImageRight = MediaImageRight;
exports.MediaImageLeft = MediaImageLeft;
exports.MediaDeleteRight = MediaDeleteRight;
exports.MediaDeleteLeft = MediaDeleteLeft;
exports.Content = Content;