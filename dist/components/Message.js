'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MessageHeader = exports.MessageBody = exports.Message = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MessageBody = function (_BulmaAnimatedCompone) {
  _inherits(MessageBody, _BulmaAnimatedCompone);

  function MessageBody() {
    _classCallCheck(this, MessageBody);

    return _possibleConstructorReturn(this, (MessageBody.__proto__ || Object.getPrototypeOf(MessageBody)).apply(this, arguments));
  }

  _createClass(MessageBody, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'message-body ' + _get(MessageBody.prototype.__proto__ || Object.getPrototypeOf(MessageBody.prototype), 'className', this);
    }
  }]);

  return MessageBody;
}(_BulmaAnimatedComponent2.default);

var MessageHeader = function (_BulmaAnimatedCompone2) {
  _inherits(MessageHeader, _BulmaAnimatedCompone2);

  function MessageHeader() {
    _classCallCheck(this, MessageHeader);

    return _possibleConstructorReturn(this, (MessageHeader.__proto__ || Object.getPrototypeOf(MessageHeader)).apply(this, arguments));
  }

  _createClass(MessageHeader, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'header',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'message-header ' + _get(MessageHeader.prototype.__proto__ || Object.getPrototypeOf(MessageHeader.prototype), 'className', this);
    }
  }]);

  return MessageHeader;
}(_BulmaAnimatedComponent2.default);

var messageClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  dark: 'is-dark',
  danger: 'is-danger'
};

var Message = function (_BulmaAnimatedCompone3) {
  _inherits(Message, _BulmaAnimatedCompone3);

  function Message() {
    _classCallCheck(this, Message);

    return _possibleConstructorReturn(this, (Message.__proto__ || Object.getPrototypeOf(Message)).apply(this, arguments));
  }

  _createClass(Message, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'article',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'messageClases',
    get: function get() {
      var _this4 = this;

      return this.messageProps.filter(function (prop) {
        return _this4.props[prop];
      }).map(function (prop) {
        return messageClases[prop];
      }).join(' ');
    }
  }, {
    key: 'messageProps',
    get: function get() {
      return Object.keys(messageClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(Message.prototype.__proto__ || Object.getPrototypeOf(Message.prototype), 'extendedProps', this)), _toConsumableArray(this.messageProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'message ' + _get(Message.prototype.__proto__ || Object.getPrototypeOf(Message.prototype), 'className', this) + ' ' + this.messageClases;
    }
  }]);

  return Message;
}(_BulmaAnimatedComponent2.default);

exports.default = Message;


Message.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  dark: false,
  danger: false
});

exports.Message = Message;
exports.MessageBody = MessageBody;
exports.MessageHeader = MessageHeader;