'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NavBarDropDown = exports.NavBarMenu = exports.NavBarLink = exports.NavBarItemLink = exports.NavBarItem = exports.NavBarBurger = exports.NavBarStart = exports.NavBarEnd = exports.NavBarContent = exports.NavBarBrand = exports.NavBarDivider = exports.NavBar = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var transaprentClases = {
  transaprent: 'is-transaprent'
};
var transaprentProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  transaprent: false
});

var NavBar = function (_BulmaAnimatedCompone) {
  _inherits(NavBar, _BulmaAnimatedCompone);

  function NavBar() {
    _classCallCheck(this, NavBar);

    return _possibleConstructorReturn(this, (NavBar.__proto__ || Object.getPrototypeOf(NavBar)).apply(this, arguments));
  }

  _createClass(NavBar, [{
    key: 'render',
    value: function render() {
      _react2.default.createElement(
        'nav',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'navBarClases',
    get: function get() {
      var _this2 = this;

      return this.burgerProps.filter(function (prop) {
        return _this2.props[prop];
      }).map(function (prop) {
        return transaprentClases[prop];
      }).join(' ');
    }
  }, {
    key: 'navBarProps',
    get: function get() {
      return Object.keys(transaprentClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(NavBar.prototype.__proto__ || Object.getPrototypeOf(NavBar.prototype), 'extendedProps', this)), _toConsumableArray(this.navBarProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'navbar ' + _get(NavBar.prototype.__proto__ || Object.getPrototypeOf(NavBar.prototype), 'className', this);
    }
  }]);

  return NavBar;
}(_BulmaAnimatedComponent2.default);

NavBar.defaultProps = transaprentProps;

var activeClases = {
  active: 'is-active'
};
var activeProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  active: false
});

var NavBarBurger = function (_BulmaAnimatedCompone2) {
  _inherits(NavBarBurger, _BulmaAnimatedCompone2);

  function NavBarBurger() {
    _classCallCheck(this, NavBarBurger);

    return _possibleConstructorReturn(this, (NavBarBurger.__proto__ || Object.getPrototypeOf(NavBarBurger)).apply(this, arguments));
  }

  _createClass(NavBarBurger, [{
    key: 'render',
    value: function render() {
      _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        _react2.default.createElement('span', null),
        _react2.default.createElement('span', null),
        _react2.default.createElement('span', null)
      );
    }
  }, {
    key: 'activeClases',
    get: function get() {
      var _this4 = this;

      return this.burgerProps.filter(function (prop) {
        return _this4.props[prop];
      }).map(function (prop) {
        return activeClases[prop];
      }).join(' ');
    }
  }, {
    key: 'burgerProps',
    get: function get() {
      return Object.keys(activeClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(NavBarBurger.prototype.__proto__ || Object.getPrototypeOf(NavBarBurger.prototype), 'extendedProps', this)), _toConsumableArray(this.burgerProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'navbar-burger ' + _get(NavBarBurger.prototype.__proto__ || Object.getPrototypeOf(NavBarBurger.prototype), 'className', this);
    }
  }]);

  return NavBarBurger;
}(_BulmaAnimatedComponent2.default);

NavBarBurger.defaultProps = activeProps;

var NavBarMenu = function (_BulmaAnimatedCompone3) {
  _inherits(NavBarMenu, _BulmaAnimatedCompone3);

  function NavBarMenu() {
    _classCallCheck(this, NavBarMenu);

    return _possibleConstructorReturn(this, (NavBarMenu.__proto__ || Object.getPrototypeOf(NavBarMenu)).apply(this, arguments));
  }

  _createClass(NavBarMenu, [{
    key: 'render',
    value: function render() {
      _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'activeClases',
    get: function get() {
      var _this6 = this;

      return this.menuProps.filter(function (prop) {
        return _this6.props[prop];
      }).map(function (prop) {
        return activeClases[prop];
      }).join(' ');
    }
  }, {
    key: 'menuProps',
    get: function get() {
      return Object.keys(activeClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(NavBarMenu.prototype.__proto__ || Object.getPrototypeOf(NavBarMenu.prototype), 'extendedProps', this)), _toConsumableArray(this.menuProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'navbar-menu ' + _get(NavBarMenu.prototype.__proto__ || Object.getPrototypeOf(NavBarMenu.prototype), 'className', this);
    }
  }]);

  return NavBarMenu;
}(_BulmaAnimatedComponent2.default);

NavBarMenu.defaultProps = activeProps;

var dropDownClases = Object.assign({}, activeClases, {
  dropdown: 'has-dropdown',
  hoverable: 'is-hoverable'
});
var dropDownProps = Object.assign({}, activeProps, {
  dropdown: false,
  hoverable: false
});

var NavBarItem = function (_BulmaAnimatedCompone4) {
  _inherits(NavBarItem, _BulmaAnimatedCompone4);

  function NavBarItem() {
    _classCallCheck(this, NavBarItem);

    return _possibleConstructorReturn(this, (NavBarItem.__proto__ || Object.getPrototypeOf(NavBarItem)).apply(this, arguments));
  }

  _createClass(NavBarItem, [{
    key: 'render',
    value: function render() {
      _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'activeClases',
    get: function get() {
      var _this8 = this;

      return this.menuProps.filter(function (prop) {
        return _this8.props[prop];
      }).map(function (prop) {
        return dropDownClases[prop];
      }).join(' ');
    }
  }, {
    key: 'menuProps',
    get: function get() {
      return Object.keys(dropDownClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(NavBarItem.prototype.__proto__ || Object.getPrototypeOf(NavBarItem.prototype), 'extendedProps', this)), _toConsumableArray(this.menuProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'navbar-menu ' + _get(NavBarItem.prototype.__proto__ || Object.getPrototypeOf(NavBarItem.prototype), 'className', this);
    }
  }]);

  return NavBarItem;
}(_BulmaAnimatedComponent2.default);

NavBarItem.defaultProps = dropDownProps;

var NavBarLink = function (_BulmaAnimatedCompone5) {
  _inherits(NavBarLink, _BulmaAnimatedCompone5);

  function NavBarLink() {
    _classCallCheck(this, NavBarLink);

    return _possibleConstructorReturn(this, (NavBarLink.__proto__ || Object.getPrototypeOf(NavBarLink)).apply(this, arguments));
  }

  _createClass(NavBarLink, [{
    key: 'render',
    value: function render() {
      _react2.default.createElement(
        'a',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'navbar-link ' + _get(NavBarLink.prototype.__proto__ || Object.getPrototypeOf(NavBarLink.prototype), 'className', this);
    }
  }]);

  return NavBarLink;
}(_BulmaAnimatedComponent2.default);

var NavBarItemLink = function (_NavBarLink) {
  _inherits(NavBarItemLink, _NavBarLink);

  function NavBarItemLink() {
    _classCallCheck(this, NavBarItemLink);

    return _possibleConstructorReturn(this, (NavBarItemLink.__proto__ || Object.getPrototypeOf(NavBarItemLink)).apply(this, arguments));
  }

  _createClass(NavBarItemLink, [{
    key: 'className',
    get: function get() {
      return 'navbar-item ' + _get(NavBarItemLink.prototype.__proto__ || Object.getPrototypeOf(NavBarItemLink.prototype), 'animatedBulmaClases', this);
    }
  }]);

  return NavBarItemLink;
}(NavBarLink);

var NavBarBrand = function (_BulmaAnimatedCompone6) {
  _inherits(NavBarBrand, _BulmaAnimatedCompone6);

  function NavBarBrand() {
    _classCallCheck(this, NavBarBrand);

    return _possibleConstructorReturn(this, (NavBarBrand.__proto__ || Object.getPrototypeOf(NavBarBrand)).apply(this, arguments));
  }

  _createClass(NavBarBrand, [{
    key: 'render',
    value: function render() {
      _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'navbar-brand ' + _get(NavBarBrand.prototype.__proto__ || Object.getPrototypeOf(NavBarBrand.prototype), 'className', this);
    }
  }]);

  return NavBarBrand;
}(_BulmaAnimatedComponent2.default);

var NavBarStart = function (_NavBarBrand) {
  _inherits(NavBarStart, _NavBarBrand);

  function NavBarStart() {
    _classCallCheck(this, NavBarStart);

    return _possibleConstructorReturn(this, (NavBarStart.__proto__ || Object.getPrototypeOf(NavBarStart)).apply(this, arguments));
  }

  _createClass(NavBarStart, [{
    key: 'className',
    get: function get() {
      return 'navbar-start ' + _get(NavBarStart.prototype.__proto__ || Object.getPrototypeOf(NavBarStart.prototype), 'animatedBulmaClases', this);
    }
  }]);

  return NavBarStart;
}(NavBarBrand);

var NavBarEnd = function (_NavBarBrand2) {
  _inherits(NavBarEnd, _NavBarBrand2);

  function NavBarEnd() {
    _classCallCheck(this, NavBarEnd);

    return _possibleConstructorReturn(this, (NavBarEnd.__proto__ || Object.getPrototypeOf(NavBarEnd)).apply(this, arguments));
  }

  _createClass(NavBarEnd, [{
    key: 'className',
    get: function get() {
      return 'navbar-end ' + _get(NavBarEnd.prototype.__proto__ || Object.getPrototypeOf(NavBarEnd.prototype), 'animatedBulmaClases', this);
    }
  }]);

  return NavBarEnd;
}(NavBarBrand);

var NavBarContent = function (_NavBarBrand3) {
  _inherits(NavBarContent, _NavBarBrand3);

  function NavBarContent() {
    _classCallCheck(this, NavBarContent);

    return _possibleConstructorReturn(this, (NavBarContent.__proto__ || Object.getPrototypeOf(NavBarContent)).apply(this, arguments));
  }

  _createClass(NavBarContent, [{
    key: 'className',
    get: function get() {
      return 'navbar-content ' + _get(NavBarContent.prototype.__proto__ || Object.getPrototypeOf(NavBarContent.prototype), 'animatedBulmaClases', this);
    }
  }]);

  return NavBarContent;
}(NavBarBrand);

var NavBarDropDown = function (_NavBarBrand4) {
  _inherits(NavBarDropDown, _NavBarBrand4);

  function NavBarDropDown() {
    _classCallCheck(this, NavBarDropDown);

    return _possibleConstructorReturn(this, (NavBarDropDown.__proto__ || Object.getPrototypeOf(NavBarDropDown)).apply(this, arguments));
  }

  _createClass(NavBarDropDown, [{
    key: 'className',
    get: function get() {
      return 'navbar-dropdown ' + _get(NavBarDropDown.prototype.__proto__ || Object.getPrototypeOf(NavBarDropDown.prototype), 'animatedBulmaClases', this);
    }
  }]);

  return NavBarDropDown;
}(NavBarBrand);

var NavBarDivider = function (_BulmaAnimatedCompone7) {
  _inherits(NavBarDivider, _BulmaAnimatedCompone7);

  function NavBarDivider() {
    _classCallCheck(this, NavBarDivider);

    return _possibleConstructorReturn(this, (NavBarDivider.__proto__ || Object.getPrototypeOf(NavBarDivider)).apply(this, arguments));
  }

  _createClass(NavBarDivider, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement('hr', _extends({}, this.nativeProps, { className: this.className, style: this.style }));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'navbar-divider ' + _get(NavBarDivider.prototype.__proto__ || Object.getPrototypeOf(NavBarDivider.prototype), 'className', this);
    }
  }]);

  return NavBarDivider;
}(_BulmaAnimatedComponent2.default);

exports.NavBar = NavBar;
exports.NavBarDivider = NavBarDivider;
exports.NavBarBrand = NavBarBrand;
exports.NavBarContent = NavBarContent;
exports.NavBarEnd = NavBarEnd;
exports.NavBarStart = NavBarStart;
exports.NavBarBurger = NavBarBurger;
exports.NavBarItem = NavBarItem;
exports.NavBarItemLink = NavBarItemLink;
exports.NavBarLink = NavBarLink;
exports.NavBarMenu = NavBarMenu;
exports.NavBarDropDown = NavBarDropDown;