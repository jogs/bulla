'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ModalClose = exports.ModalBackground = exports.ModalContent = exports.Modal = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaAnimatedComponent = require('../core/BulmaAnimatedComponent');

var _BulmaAnimatedComponent2 = _interopRequireDefault(_BulmaAnimatedComponent);

var _Delete2 = require('../elements/Delete');

var _Delete3 = _interopRequireDefault(_Delete2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ModalBackground = function (_BulmaAnimatedCompone) {
  _inherits(ModalBackground, _BulmaAnimatedCompone);

  function ModalBackground() {
    _classCallCheck(this, ModalBackground);

    return _possibleConstructorReturn(this, (ModalBackground.__proto__ || Object.getPrototypeOf(ModalBackground)).apply(this, arguments));
  }

  _createClass(ModalBackground, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement('div', _extends({}, this.nativeProps, { className: this.className, style: this.style }));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'modal-background ' + _get(ModalBackground.prototype.__proto__ || Object.getPrototypeOf(ModalBackground.prototype), 'className', this);
    }
  }]);

  return ModalBackground;
}(_BulmaAnimatedComponent2.default);

var ModalClose = function (_Delete) {
  _inherits(ModalClose, _Delete);

  function ModalClose() {
    _classCallCheck(this, ModalClose);

    return _possibleConstructorReturn(this, (ModalClose.__proto__ || Object.getPrototypeOf(ModalClose)).apply(this, arguments));
  }

  _createClass(ModalClose, [{
    key: 'className',
    get: function get() {
      return 'modal-close ' + _get(ModalClose.prototype.__proto__ || Object.getPrototypeOf(ModalClose.prototype), 'animatedBulmaClases', this) + ' ' + _get(ModalClose.prototype.__proto__ || Object.getPrototypeOf(ModalClose.prototype), 'deleteClases', this);
    }
  }]);

  return ModalClose;
}(_Delete3.default);

var ModalContent = function (_BulmaAnimatedCompone2) {
  _inherits(ModalContent, _BulmaAnimatedCompone2);

  function ModalContent() {
    _classCallCheck(this, ModalContent);

    return _possibleConstructorReturn(this, (ModalContent.__proto__ || Object.getPrototypeOf(ModalContent)).apply(this, arguments));
  }

  _createClass(ModalContent, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        _extends({}, this.nativeProps, { className: this.className, style: this.style }),
        this.props.children
      );
    }
  }, {
    key: 'className',
    get: function get() {
      return 'modal-content ' + _get(ModalContent.prototype.__proto__ || Object.getPrototypeOf(ModalContent.prototype), 'className', this);
    }
  }]);

  return ModalContent;
}(_BulmaAnimatedComponent2.default);

var modalClases = {
  active: 'is-active'
};

var Modal = function (_ModalContent) {
  _inherits(Modal, _ModalContent);

  function Modal() {
    _classCallCheck(this, Modal);

    return _possibleConstructorReturn(this, (Modal.__proto__ || Object.getPrototypeOf(Modal)).apply(this, arguments));
  }

  _createClass(Modal, [{
    key: 'modalClases',
    get: function get() {
      var _this5 = this;

      return this.modalProps.filter(function (prop) {
        return _this5.props[prop];
      }).map(function (prop) {
        return modalClases[prop];
      }).join(' ');
    }
  }, {
    key: 'modalProps',
    get: function get() {
      return Object.keys(modalClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(Modal.prototype.__proto__ || Object.getPrototypeOf(Modal.prototype), 'extendedProps', this)), _toConsumableArray(this.modalProps));
    }
  }, {
    key: 'className',
    get: function get() {
      return 'modal ' + _get(Modal.prototype.__proto__ || Object.getPrototypeOf(Modal.prototype), 'animatedBulmaClases', this) + ' ' + this.modalClases;
    }
  }]);

  return Modal;
}(ModalContent);

Modal.defaultProps = Object.assign({}, _BulmaAnimatedComponent2.default.defaultProps, {
  active: false
});

exports.Modal = Modal;
exports.ModalContent = ModalContent;
exports.ModalBackground = ModalBackground;
exports.ModalClose = ModalClose;