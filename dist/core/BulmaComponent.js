'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _lodash = require('lodash');

require('bulma/css/bulma.css');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var bulmaClases = {
  textCentered: 'has-text-centered',
  textLeft: 'has-text-left',
  textRight: 'has-text-right',
  clearfix: 'is-clearfix',
  pulledLeft: 'is-pulled-left',
  pulledRight: 'is-pulled-right',
  overlay: 'is-overlay',
  fullwidth: 'is-fullwidth',
  marginless: 'is-marginless',
  paddingless: 'is-paddingless',
  unselectable: 'is-unselectable',
  hidden: 'is-hidden'
};

var BulmaComponent = function (_Component) {
  _inherits(BulmaComponent, _Component);

  function BulmaComponent() {
    _classCallCheck(this, BulmaComponent);

    return _possibleConstructorReturn(this, (BulmaComponent.__proto__ || Object.getPrototypeOf(BulmaComponent)).apply(this, arguments));
  }

  _createClass(BulmaComponent, [{
    key: '_bulmaClases',
    get: function get() {
      return bulmaClases;
    }
  }, {
    key: 'bulmaClases',
    get: function get() {
      var _this2 = this;

      return this.bulmaProps.filter(function (prop) {
        return _this2.props[prop];
      }).map(function (prop) {
        return _this2._bulmaClases[prop];
      }).join(' ');
    }
  }, {
    key: 'bulmaProps',
    get: function get() {
      return Object.keys(this._bulmaClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return this.bulmaProps;
    }
  }, {
    key: 'className',
    get: function get() {
      return this.props.className + ' ' + this.bulmaClases;
    }
  }, {
    key: 'nativeProps',
    get: function get() {
      var _this3 = this;

      return (0, _lodash.difference)(Object.keys(this.props), [].concat(_toConsumableArray(this.extendedProps), ['children'])).reduce(function (objectPrev, currentProp) {
        var tmp = {};
        tmp[currentProp] = _this3.props[currentProp];
        return Object.assign({}, objectPrev, tmp);
      }, {});
    }
  }]);

  return BulmaComponent;
}(_react.Component);

exports.default = BulmaComponent;


BulmaComponent.defaultProps = {
  className: '',
  textCentered: false,
  textLeft: false,
  textRight: false,
  clearfix: false,
  pulledLeft: false,
  pulledRight: false,
  overlay: false,
  fullwidth: false,
  marginless: false,
  paddingless: false,
  unselectable: false,
  hidden: false
};