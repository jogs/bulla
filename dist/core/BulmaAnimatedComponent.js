'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BulmaComponent2 = require('./BulmaComponent');

var _BulmaComponent3 = _interopRequireDefault(_BulmaComponent2);

require('animate.css/animate.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var animatedClases = {
  infinite: 'infinite',
  bounce: 'bounce',
  flash: 'flash',
  pulse: 'pulse',
  rubberBand: 'rubberBand',
  shake: 'shake',
  headShake: 'headShake',
  swing: 'swing',
  tada: 'tada',
  wobble: 'wobble',
  jello: 'jello',
  bounceIn: 'bounceIn',
  bounceInDown: 'bounceInDown',
  bounceInLeft: 'bounceInLeft',
  bounceInRight: 'bounceInRight',
  bounceInUp: 'bounceInUp',
  bounceOut: 'bounceOut',
  bounceOutDown: 'bounceOutDown',
  bounceOutLeft: 'bounceOutLeft',
  bounceOutRight: 'bounceOutRight',
  bounceOutUp: 'bounceOutUp',
  fadeIn: 'fadeIn',
  fadeInDown: 'fadeInDown',
  fadeInDownBig: 'fadeInDownBig',
  fadeInLeft: 'fadeInLeft',
  fadeInLeftBig: 'fadeInLeftBig',
  fadeInRight: 'fadeInRight',
  fadeInRightBig: 'fadeInRightBig',
  fadeInUp: 'fadeInUp',
  fadeInUpBig: 'fadeInUpBig',
  fadeOut: 'fadeOut',
  fadeOutDown: 'fadeOutDown',
  fadeOutDownBig: 'fadeOutDownBig',
  fadeOutLeft: 'fadeOutLeft',
  fadeOutLeftBig: 'fadeOutLeftBig',
  fadeOutRight: 'fadeOutRight',
  fadeOutRightBig: 'fadeOutRightBig',
  fadeOutUp: 'fadeOutUp',
  fadeOutUpBig: 'fadeOutUpBig',
  flipInX: 'flipInX',
  flipInY: 'flipInY',
  flipOutX: 'flipOutX',
  flipOutY: 'flipOutY',
  lightSpeedIn: 'lightSpeedIn',
  lightSpeedOut: 'lightSpeedOut',
  rotateIn: 'rotateIn',
  rotateInDownLeft: 'rotateInDownLeft',
  rotateInDownRight: 'rotateInDownRight',
  rotateInUpLeft: 'rotateInUpLeft',
  rotateInUpRight: 'rotateInUpRight',
  rotateOut: 'rotateOut',
  rotateOutDownLeft: 'rotateOutDownLeft',
  rotateOutDownRight: 'rotateOutDownRight',
  rotateOutUpLeft: 'rotateOutUpLeft',
  rotateOutUpRight: 'rotateOutUpRight',
  hinge: 'hinge',
  jackInTheBox: 'jackInTheBox',
  rollIn: 'rollIn',
  rollOut: 'rollOut',
  zoomIn: 'zoomIn',
  zoomInDown: 'zoomInDown',
  zoomInLeft: 'zoomInLeft',
  zoomInRight: 'zoomInRight',
  zoomInUp: 'zoomInUp',
  zoomOut: 'zoomOut',
  zoomOutDown: 'zoomOutDown',
  zoomOutLeft: 'zoomOutLeft',
  zoomOutRight: 'zoomOutRight',
  zoomOutUp: 'zoomOutUp',
  slideInDown: 'slideInDown',
  slideInLeft: 'slideInLeft',
  slideInRight: 'slideInRight',
  slideInUp: 'slideInUp',
  slideOutDown: 'slideOutDown',
  slideOutLeft: 'slideOutLeft',
  slideOutRight: 'slideOutRight',
  slideOutUp: 'slideOutUp'
};

var AnimatedBulmaComponent = function (_BulmaComponent) {
  _inherits(AnimatedBulmaComponent, _BulmaComponent);

  function AnimatedBulmaComponent() {
    _classCallCheck(this, AnimatedBulmaComponent);

    return _possibleConstructorReturn(this, (AnimatedBulmaComponent.__proto__ || Object.getPrototypeOf(AnimatedBulmaComponent)).apply(this, arguments));
  }

  _createClass(AnimatedBulmaComponent, [{
    key: 'animatedClases',
    get: function get() {
      var _this2 = this;

      return this.animatedProps.filter(function (prop) {
        return _this2.props[prop];
      }).map(function (prop) {
        return animatedClases[prop];
      }).join(' ');
    }
  }, {
    key: 'animatedProps',
    get: function get() {
      return Object.keys(animatedClases);
    }
  }, {
    key: 'extendedProps',
    get: function get() {
      return [].concat(_toConsumableArray(_get(AnimatedBulmaComponent.prototype.__proto__ || Object.getPrototypeOf(AnimatedBulmaComponent.prototype), 'extendedProps', this)), _toConsumableArray(this.animatedProps), ['delay']);
    }
  }, {
    key: 'className',
    get: function get() {
      return this.animatedBulmaClasses;
    }
  }, {
    key: 'animatedBulmaClasses',
    get: function get() {
      return 'animated ' + _get(AnimatedBulmaComponent.prototype.__proto__ || Object.getPrototypeOf(AnimatedBulmaComponent.prototype), 'className', this) + ' ' + this.animatedClases;
    }
  }, {
    key: 'style',
    get: function get() {
      return Object.assign({}, this.props.style, {
        animationDelay: this.props.delay + 'ms'
      });
    }
  }]);

  return AnimatedBulmaComponent;
}(_BulmaComponent3.default);

exports.default = AnimatedBulmaComponent;


AnimatedBulmaComponent.defaultProps = Object.assign({}, _BulmaComponent3.default.defaultProps, {
  delay: 0,
  infinite: false,
  bounce: false,
  flash: false,
  pulse: false,
  rubberBand: false,
  shake: false,
  headShake: false,
  swing: false,
  tada: false,
  wobble: false,
  jello: false,
  bounceIn: false,
  bounceInDown: false,
  bounceInLeft: false,
  bounceInRight: false,
  bounceInUp: false,
  bounceOut: false,
  bounceOutDown: false,
  bounceOutLeft: false,
  bounceOutRight: false,
  bounceOutUp: false,
  fadeIn: false,
  fadeInDown: false,
  fadeInDownBig: false,
  fadeInLeft: false,
  fadeInLeftBig: false,
  fadeInRight: false,
  fadeInRightBig: false,
  fadeInUp: false,
  fadeInUpBig: false,
  fadeOut: false,
  fadeOutDown: false,
  fadeOutDownBig: false,
  fadeOutLeft: false,
  fadeOutLeftBig: false,
  fadeOutRight: false,
  fadeOutRightBig: false,
  fadeOutUp: false,
  fadeOutUpBig: false,
  flipInX: false,
  flipInY: false,
  flipOutX: false,
  flipOutY: false,
  lightSpeedIn: false,
  lightSpeedOut: false,
  rotateIn: false,
  rotateInDownLeft: false,
  rotateInDownRight: false,
  rotateInUpLeft: false,
  rotateInUpRight: false,
  rotateOut: false,
  rotateOutDownLeft: false,
  rotateOutDownRight: false,
  rotateOutUpLeft: false,
  rotateOutUpRight: false,
  hinge: false,
  jackInTheBox: false,
  rollIn: false,
  rollOut: false,
  zoomIn: false,
  zoomInDown: false,
  zoomInLeft: false,
  zoomInRight: false,
  zoomInUp: false,
  zoomOut: false,
  zoomOutDown: false,
  zoomOutLeft: false,
  zoomOutRight: false,
  zoomOutUp: false,
  slideInDown: false,
  slideInLeft: false,
  slideInRight: false,
  slideInUp: false,
  slideOutDown: false,
  slideOutLeft: false,
  slideOutRight: false,
  slideOutUp: false
});