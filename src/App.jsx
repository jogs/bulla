// import {Tile, Columns, Column} from './elements/Grid'
import {Icon} from './elements/Icon'
import Button from './elements/Button'
import Box from './elements/Box'
import Delete from './elements/Delete'
import Notification from './elements/Notification'
import Progress from './elements/Progress'
import Tag from './elements/Tag'
import Table from './elements/Table'
import React, {Component} from 'react'

export default class App extends Component {
  render () {
    return (
      <div className={'container'}>
        <Box fadeInDownBig>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </Box>
        <Notification info fadeInLeftBig>
          <Delete />
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </Notification>
        <Button fadeInUpBig>
          <Icon name={'check'} small />
          <span>OK</span>
        </Button>
        <Tag danger fadeInRightBig>TEXT <Delete /></Tag>
        <Progress fadeIn value={50} max={100} />
        <Table narrow striped>
          <thead>
            <tr>
              <th>UNO</th>
              <th>DOS</th>
              <th>TRES</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1.1</td>
              <td>1.2</td>
              <td>1.3</td>
            </tr>
            <tr>
              <td>2.1</td>
              <td>2.2</td>
              <td>2.3</td>
            </tr>
            <tr>
              <td>3.1</td>
              <td>3.2</td>
              <td>3.3</td>
            </tr>
            <tr>
              <td>4.1</td>
              <td>4.2</td>
              <td>4.3</td>
            </tr>
          </tbody>
        </Table>
      </div>
    )
  }
}
