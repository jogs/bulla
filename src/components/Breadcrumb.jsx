import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const breadcrumbClases = {
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large',
  centered: 'is-centered',
  right: 'is-right',
  arrow: 'has-arrow-separator',
  bullet: 'has-bullet-separator',
  dot: 'has-dot-separator',
  succeeds: 'has-succeeds-separator'
}

class Breadcrumb extends BulmaAnimatedComponent {
  get breadcrumbClases () {
    return this.breadcrumbProps.filter(prop => this.props[prop])
    .map(prop => breadcrumbClases[prop])
    .join(' ')
  }
  get breadcrumbProps () {
    return Object.keys(breadcrumbClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.breadcrumbProps]
  }
  get className () {
    return `breadcrumb ${super.className} ${this.breadcrumbClases}`
  }
  render () {
    return (
      <nav {...this.nativeProps} className={this.className} style={this.style} >
        <ul>
          {this.props.children}
        </ul>
      </nav>
    )
  }
}

const elementClases = {
  active: 'is-active'
}

class BreadcrumbElement extends BulmaAnimatedComponent {
  get elementClases () {
    return this.elementProps.filter(prop => this.props[prop])
    .map(prop => elementClases[prop])
    .join(' ')
  }
  get elementProps () {
    return Object.keys(elementClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.elementProps]
  }
  get className () {
    return `${super.className} ${this.elementClases}`
  }
  render () {
    return (
      <li {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </li>
    )
  }
}

Breadcrumb.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  small: false,
  medium: false,
  large: false,
  centered: false,
  right: false,
  arrow: false,
  bullet: false,
  dot: false,
  succeeds: false
})

BreadcrumbElement.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  active: false
})

export {
  Breadcrumb,
  BreadcrumbElement
}
