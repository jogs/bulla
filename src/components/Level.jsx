import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

class Level extends BulmaAnimatedComponent {
  get className () {
    return `level ${super.className}`
  }
  render () {
    return (
      <nav {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </nav>
    )
  }
}

class LevelRight extends BulmaAnimatedComponent {
  get className () {
    return `level-right ${super.className}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </div>
    )
  }
}

class LevelLeft extends LevelRight {
  get className () {
    return `level-left ${super.animatedBulmaClases}`
  }
}

const levelClases = {
  mobile: 'is-mobile'
}

class LevelItem extends BulmaAnimatedComponent {
  get levelClases () {
    return this.levelProps.filter(prop => this.props[prop])
    .map(prop => levelClases[prop])
    .join(' ')
  }
  get levelProps () {
    return Object.keys(levelClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.levelProps]
  }
  get className () {
    return `level-item ${super.className} ${this.levelClases}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </div>
    )
  }
}

LevelItem.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  mobile: false
})

export {
  Level,
  LevelRight,
  LevelLeft,
  LevelItem
}
