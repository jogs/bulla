import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import Delete from '../elements/Delete'
import React from 'react'

class ModalBackground extends BulmaAnimatedComponent {
  get className () {
    return `modal-background ${super.className}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style} />
    )
  }
}

class ModalClose extends Delete {
  get className () {
    return `modal-close ${super.animatedBulmaClases} ${super.deleteClases}`
  }
}

class ModalContent extends BulmaAnimatedComponent {
  get className () {
    return `modal-content ${super.className}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </div>
    )
  }
}

const modalClases = {
  active: 'is-active'
}

class Modal extends ModalContent {
  get modalClases () {
    return this.modalProps.filter(prop => this.props[prop])
    .map(prop => modalClases[prop])
    .join(' ')
  }
  get modalProps () {
    return Object.keys(modalClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.modalProps]
  }
  get className () {
    return `modal ${super.animatedBulmaClases} ${this.modalClases}`
  }
}

Modal.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  active: false
})

export {
  Modal,
  ModalContent,
  ModalBackground,
  ModalClose
}
