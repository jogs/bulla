import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const transaprentClases = {
  transaprent: 'is-transaprent'
}
const transaprentProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  transaprent: false
})

class NavBar extends BulmaAnimatedComponent {
  get navBarClases () {
    return this.burgerProps.filter(prop => this.props[prop])
    .map(prop => transaprentClases[prop])
    .join(' ')
  }
  get navBarProps () {
    return Object.keys(transaprentClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.navBarProps]
  }
  get className () {
    return `navbar ${super.className}`
  }
  render () {
    <nav {...this.nativeProps} className={this.className} style={this.style}>
      {this.props.children}
    </nav>
  }
}

NavBar.defaultProps = transaprentProps

const activeClases = {
  active: 'is-active'
}
const activeProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  active: false
})

class NavBarBurger extends BulmaAnimatedComponent {
  get activeClases () {
    return this.burgerProps.filter(prop => this.props[prop])
    .map(prop => activeClases[prop])
    .join(' ')
  }
  get burgerProps () {
    return Object.keys(activeClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.burgerProps]
  }
  get className () {
    return `navbar-burger ${super.className}`
  }
  render () {
    <div {...this.nativeProps} className={this.className} style={this.style}>
      <span />
      <span />
      <span />
    </div>
  }
}

NavBarBurger.defaultProps = activeProps

class NavBarMenu extends BulmaAnimatedComponent {
  get activeClases () {
    return this.menuProps.filter(prop => this.props[prop])
    .map(prop => activeClases[prop])
    .join(' ')
  }
  get menuProps () {
    return Object.keys(activeClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.menuProps]
  }
  get className () {
    return `navbar-menu ${super.className}`
  }
  render () {
    <div {...this.nativeProps} className={this.className} style={this.style}>
      {this.props.children}
    </div>
  }
}

NavBarMenu.defaultProps = activeProps

const dropDownClases = Object.assign({}, activeClases, {
  dropdown: 'has-dropdown',
  hoverable: 'is-hoverable'
})
const dropDownProps = Object.assign({}, activeProps, {
  dropdown: false,
  hoverable: false
})

class NavBarItem extends BulmaAnimatedComponent {
  get activeClases () {
    return this.menuProps.filter(prop => this.props[prop])
    .map(prop => dropDownClases[prop])
    .join(' ')
  }
  get menuProps () {
    return Object.keys(dropDownClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.menuProps]
  }
  get className () {
    return `navbar-menu ${super.className}`
  }
  render () {
    <div {...this.nativeProps} className={this.className} style={this.style}>
      {this.props.children}
    </div>
  }
}

NavBarItem.defaultProps = dropDownProps

class NavBarLink extends BulmaAnimatedComponent {
  get className () {
    return `navbar-link ${super.className}`
  }
  render () {
    <a {...this.nativeProps} className={this.className} style={this.style}>
      {this.props.children}
    </a>
  }
}

class NavBarItemLink extends NavBarLink {
  get className () {
    return `navbar-item ${super.animatedBulmaClases}`
  }
}

class NavBarBrand extends BulmaAnimatedComponent {
  get className () {
    return `navbar-brand ${super.className}`
  }
  render () {
    <div {...this.nativeProps} className={this.className} style={this.style}>
      {this.props.children}
    </div>
  }
}

class NavBarStart extends NavBarBrand {
  get className () {
    return `navbar-start ${super.animatedBulmaClases}`
  }
}

class NavBarEnd extends NavBarBrand {
  get className () {
    return `navbar-end ${super.animatedBulmaClases}`
  }
}

class NavBarContent extends NavBarBrand {
  get className () {
    return `navbar-content ${super.animatedBulmaClases}`
  }
}

class NavBarDropDown extends NavBarBrand {
  get className () {
    return `navbar-dropdown ${super.animatedBulmaClases}`
  }
}

class NavBarDivider extends BulmaAnimatedComponent {
  get className () {
    return `navbar-divider ${super.className}`
  }
  render () {
    return (
      <hr {...this.nativeProps} className={this.className} style={this.style} />
    )
  }
}

export {
  NavBar,
  NavBarDivider,
  NavBarBrand,
  NavBarContent,
  NavBarEnd,
  NavBarStart,
  NavBarBurger,
  NavBarItem,
  NavBarItemLink,
  NavBarLink,
  NavBarMenu,
  NavBarDropDown
}
