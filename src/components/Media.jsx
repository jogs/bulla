import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import {Image, Delete} from '../elements'
import React from 'react'

class Media extends BulmaAnimatedComponent {
  get className () {
    return `media ${super.className}`
  }
  render () {
    return (
      <article {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </article>
    )
  }
}

class MediaContent extends BulmaAnimatedComponent {
  get className () {
    return `media-content ${super.className}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </div>
    )
  }
}

class MediaImageLeft extends Image {
  render () {
    return (
      <figure className={'media-left'}>
        <p className={this.className} style={this.style}>
          <img {...this.nativeProps} />
        </p>
      </figure>
    )
  }
}

class MediaImageRight extends Image {
  render () {
    return (
      <figure className={'media-right'}>
        <p className={this.className} style={this.style}>
          <img {...this.nativeProps} />
        </p>
      </figure>
    )
  }
}

class MediaDeleteRight extends BulmaAnimatedComponent {
  get className () {
    return `media-right ${super.className}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style}>
        <Delete />
      </div>
    )
  }
}

class MediaDeleteLeft extends MediaDeleteRight {
  get className () {
    return `media-left ${super.animatedBulmaClasses}`
  }
}

class Content extends MediaContent {
  get className () {
    return `content ${super.animatedBulmaClasses}`
  }
}

export {
  Media,
  MediaContent,
  MediaImageRight,
  MediaImageLeft,
  MediaDeleteRight,
  MediaDeleteLeft,
  Content
}
