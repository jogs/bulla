import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

class Menu extends BulmaAnimatedComponent {
  get className () {
    return `menu ${super.className}`
  }
  render () {
    return (
      <aside {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </aside>
    )
  }
}

class MenuList extends BulmaAnimatedComponent {
  get className () {
    return `menu-list ${super.className}`
  }
  render () {
    return (
      <ul {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </ul>
    )
  }
}

class MenuLabel extends BulmaAnimatedComponent {
  get className () {
    return `menu-label ${super.className}`
  }
  render () {
    return (
      <p {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </p>
    )
  }
}

export {
  Menu,
  MenuList,
  MenuLabel
}
