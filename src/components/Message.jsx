import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

class MessageBody extends BulmaAnimatedComponent {
  get className () {
    return `message-body ${super.className}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </div>
    )
  }
}

class MessageHeader extends BulmaAnimatedComponent {
  get className () {
    return `message-header ${super.className}`
  }
  render () {
    return (
      <header {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </header>
    )
  }
}

const messageClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  dark: 'is-dark',
  danger: 'is-danger'
}

export default class Message extends BulmaAnimatedComponent {
  get messageClases () {
    return this.messageProps.filter(prop => this.props[prop])
    .map(prop => messageClases[prop])
    .join(' ')
  }
  get messageProps () {
    return Object.keys(messageClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.messageProps]
  }
  get className () {
    return `message ${super.className} ${this.messageClases}`
  }
  render () {
    return (
      <article {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </article>
    )
  }
}

Message.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  dark: false,
  danger: false
})

export {
  Message,
  MessageBody,
  MessageHeader
}
