import InputField from './InputField'

export default class TextField extends InputField {
  get className () {
    return `textarea ${super.className} ${this.inputClases}`
  }
  render () {
    return (
      <textarea {...this.nativeProps} className={this.className} style={this.style} />
    )
  }
}
