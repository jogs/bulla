import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

export default class Label extends BulmaAnimatedComponent {
  get className () {
    return `label ${super.className}`
  }
  render () {
    return (
      <label {...this.nativeProps} className={this.className} style={this.style}>
        {this.props.children}
      </label>
    )
  }
}
