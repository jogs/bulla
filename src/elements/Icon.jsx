import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'
import 'font-awesome/css/font-awesome.css'

class SimpleIcon extends BulmaAnimatedComponent {
  get extendedProps () {
    return [...super.extendedProps, 'name']
  }
  get className () {
    return `${super.className} fa fa-${this.props.name}`
  }
  render () {
    return (
      <i {...this.nativeProps} className={this.className} style={this.style} />
    )
  }
}

SimpleIcon.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  name: 'bulma'
})

const iconClases = {
  left: 'is-left',
  right: 'is-right',
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large'
}

class Icon extends BulmaAnimatedComponent {
  get iconClases () {
    return this.iconProps.filter(prop => this.props[prop])
    .map(prop => iconClases[prop])
    .join(' ')
  }
  get iconProps () {
    return Object.keys(iconClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.iconProps]
  }
  get className () {
    return `icon ${super.className} ${this.iconClases}`
  }
  render () {
    return (
      <span
        className={this.className}
        style={this.style}
      >
        <i {...this.nativeProps} className={`fa fa-${this.props.name}`} />
      </span>
    )
  }
}

Icon.defaultProps = Object.assign({}, SimpleIcon.defaultProps, {
  left: false,
  right: false,
  small: false,
  medium: false,
  large: false
})

export {
  Icon,
  SimpleIcon
}
