import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const progressClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  danger: 'is-danger',
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large'
}

export default class Progress extends BulmaAnimatedComponent {
  get progressClases () {
    return this.progressProps.filter(prop => this.props[prop])
    .map(prop => progressClases[prop])
    .join(' ')
  }
  get progressProps () {
    return Object.keys(progressClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.progressProps]
  }
  get className () {
    return `progress ${super.className} ${this.progressClases}`
  }
  render () {
    return (
      <progress {...this.nativeProps} className={this.className} style={this.style} />
    )
  }
}

Progress.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  danger: false,
  small: false,
  medium: false,
  large: false
})
