import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const tableClases = {
  bordered: 'is-bordered',
  striped: 'is-striped',
  narrow: 'is-narrow'
}

export default class Table extends BulmaAnimatedComponent {
  get tableClases () {
    return this.tableProps.filter(prop => this.props[prop])
    .map(prop => tableClases[prop])
    .join(' ')
  }
  get tableProps () {
    return Object.keys(tableClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.tableProps]
  }
  get className () {
    return `table ${super.className} ${this.tableClases}`
  }
  render () {
    return (
      <table {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </table>
    )
  }
}

Table.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  bordered: false,
  striped: false,
  narrow: false
})
