import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const inputClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  danger: 'is-danger',
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large',
  focused: 'is-focused',
  hovered: 'is-hovered',
  loading: 'is-loading'
}

export default class InputField extends BulmaAnimatedComponent {
  get inputClases () {
    return this.inputProps.filter(prop => this.props[prop])
    .map(prop => inputClases[prop])
    .join(' ')
  }
  get inputProps () {
    return Object.keys(inputClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.inputProps]
  }
  get className () {
    return `input ${super.className} ${this.inputClases}`
  }
  render () {
    return (
      <input {...this.nativeProps} className={this.className} style={this.style} />
    )
  }
}

InputField.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  danger: false,
  small: false,
  medium: false,
  large: false,
  focused: false,
  hovered: false,
  loading: false
})
