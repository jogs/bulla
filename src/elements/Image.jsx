import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const imageClases = {
  'is16x16': 'is-16x16',
  'is32x32': 'is-32x32',
  'is48x48': 'is-48x48',
  'is64x64': 'is-64x64',
  'is96x96': 'is-96x96',
  'is128x96': 'is-128x128',
  'is1by1': 'is-1by1',
  'is4by3': 'is-4by3',
  'is3by2': 'is-3by2',
  'is16by9': 'is-16by9',
  'is2by1': 'is-2by1'
}

export default class Image extends BulmaAnimatedComponent {
  get imageClases () {
    return this.imageProps.filter(prop => this.props[prop])
    .map(prop => imageClases[prop])
    .join(' ')
  }
  get imageProps () {
    return Object.keys(imageClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.imageProps]
  }
  get className () {
    return `image ${super.className} ${this.imageClases}`
  }
  render () {
    return (
      <figure className={this.className} style={this.style}>
        <img {...this.nativeProps} />
      </figure>
    )
  }
}

Image.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  'is16x16': false,
  'is32x32': false,
  'is48x48': false,
  'is64x64': false,
  'is96x96': false,
  'is128x96': false,
  'is1by1': false,
  'is4by3': false,
  'is3by2': false,
  'is16by9': false,
  'is2by1': false
})
