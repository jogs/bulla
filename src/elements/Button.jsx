import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const buttonClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  danger: 'is-danger',
  white: 'is-white',
  light: 'is-light',
  dark: 'is-dark',
  black: 'is-black',
  link: 'is-link',
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large',
  outlined: 'is-outlined',
  inverted: 'is-inverted',
  static: 'is-static',
  focused: 'is-focused',
  hovered: 'is-hovered',
  loading: 'is-loading'
}

export default class Button extends BulmaAnimatedComponent {
  get buttonClases () {
    return this.buttonProps.filter(prop => this.props[prop])
    .map(prop => buttonClases[prop])
    .join(' ')
  }
  get buttonProps () {
    return Object.keys(buttonClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.buttonProps]
  }
  get className () {
    return `button ${super.className} ${this.buttonClases}`
  }
  render () {
    return (
      <button {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </button>
    )
  }
}

Button.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  danger: false,
  white: false,
  light: false,
  dark: false,
  black: false,
  link: false,
  small: false,
  medium: false,
  large: false,
  outlined: false,
  inverted: false,
  static: false,
  focused: false,
  hovered: false,
  loading: false
})
