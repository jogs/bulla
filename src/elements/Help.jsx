import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const helpClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  danger: 'is-danger'
}

export default class Help extends BulmaAnimatedComponent {
  get helpClases () {
    return this.helpProps.filter(prop => this.props[prop])
    .map(prop => helpClases[prop])
    .join(' ')
  }
  get helpProps () {
    return Object.keys(helpClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.helpProps]
  }
  get className () {
    return `help ${super.className} ${this.helpClases}`
  }
  render () {
    return (
      <span {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </span>
    )
  }
}

Help.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  danger: false
})
