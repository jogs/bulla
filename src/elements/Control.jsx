import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const controlClases = {
  iconsLeft: 'has-icons-left',
  iconsRight: 'has-icons-right',
  expanded: 'is-expanded',
  loading: 'is-loading',
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large'
}

export default class Control extends BulmaAnimatedComponent {
  get controlClases () {
    return this.controlProps.filter(prop => this.props[prop])
    .map(prop => controlClases[prop])
    .join(' ')
  }
  get controlProps () {
    return Object.keys(controlClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.controlProps]
  }
  get className () {
    return `control ${super.className} ${this.controlClases}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </div>
    )
  }
}

Control.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  iconsLeft: false,
  iconsRight: false,
  expanded: false,
  loading: false,
  small: false,
  medium: false,
  large: false
})
