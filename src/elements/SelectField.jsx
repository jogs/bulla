import React from 'react'
import InputField from './InputField'

export default class SelectField extends InputField {
  get className () {
    return super.className.replace('input', 'select')
  }
  render () {
    return (
      <div className={this.className} style={this.style} >
        <select {...this.nativeProps}>
          {this.props.children}
        </select>
      </div>
    )
  }
}
