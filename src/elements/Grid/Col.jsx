import Grid from './Grid'
import BulmaComponent from '../../core/BulmaComponent'
import React from 'react'

const columnClases = {
  desktop: 'is-desktop',
  tablet: 'is-tablet',
  mobile: 'is-mobile',
  multiline: 'is-multiline',
  narrow: 'is-narrow',
  gapless: 'is-gapless'
}

export class Columns extends BulmaComponent {
  render () {
    return (
      <div {...this.nativeProps} className={`${this.className} columns`}>
        {this.props.children}
      </div>
    )
  }
}

export class Column extends Grid {
  get _bulmaClases () {
    return Object.assign({}, super._bulmaClases, columnClases)
  }
  render () {
    return (
      <div {...this.nativeProps} className={`${this.className} column`}>
        {this.props.children}
      </div>
    )
  }
}
