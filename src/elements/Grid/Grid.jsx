import BulmaComponent from '../../core/BulmaComponent'

export default class Grid extends BulmaComponent {
  get className () {
    return `${super.className}${this.props.hasOwnProperty('size') ? ` is-${this.props.size}` : ''}`
  }
  get extendedProps () {
    return [...super.extendedProps, 'size']
  }
}
