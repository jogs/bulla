import React from 'react'
import Grid from './Grid'

const tileClases = {
  ancestor: 'is-ancestor',
  parent: 'is-parent',
  child: 'is-child',
  vertical: 'is-vertical'
}

export default class Tile extends Grid {
  get _bulmaClases () {
    return Object.assign({}, super._bulmaClases, tileClases)
  }
  render () {
    return (
      <div {...this.nativeProps} className={`tile ${this.className}`}>
        {this.props.children}
      </div>
    )
  }
}

Tile.defaultProps = Object.assign({}, Grid.defaultProps, {
  ancestor: false,
  parent: false,
  child: false,
  vertical: false
})
