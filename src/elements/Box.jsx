import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

export default class Box extends BulmaAnimatedComponent {
  get className () {
    return `box ${super.className}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </div>
    )
  }
}
