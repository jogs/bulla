import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

export default class Delete extends BulmaAnimatedComponent {
  constructor (props) {
    super(props)
    this._deleteClases = {
      small: 'is-small',
      medium: 'is-medium',
      large: 'is-large'
    }
  }
  get deleteClases () {
    return this.deleteProps.filter(prop => this.props[prop])
    .map(prop => this._deleteClases[prop])
    .join(' ')
  }
  get deleteProps () {
    return Object.keys(this._deleteClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.deleteProps]
  }
  get className () {
    return `delete ${super.className} ${this.deleteClases}`
  }
  render () {
    return (
      <button {...this.nativeProps} className={this.className} style={this.style} />
    )
  }
}

Delete.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  small: false,
  medium: false,
  large: false
})
