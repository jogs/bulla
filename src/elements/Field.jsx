import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const fieldClases = {
  addons: 'has-addons',
  addonsCentered: 'has-addons has-addons-centered',
  addonsRight: 'has-addons has-addons-right',
  grouped: 'is-grouped',
  groupedCentered: 'is-grouped is-grouped-centered',
  groupedRight: 'is-grouped is-grouped-right',
  horizontal: 'is-horizontal'
}

export default class Field extends BulmaAnimatedComponent {
  get fieldClases () {
    return this.fieldProps.filter(prop => this.props[prop])
    .map(prop => fieldClases[prop])
    .join(' ')
  }
  get fieldProps () {
    return Object.keys(fieldClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.fieldProps]
  }
  get className () {
    return `field ${super.className} ${this.fieldClases}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </div>
    )
  }
}

Field.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  addons: false,
  addonsCentered: false,
  addonsRight: false,
  grouped: false,
  groupedCentered: false,
  groupedRight: false,
  horizontal: false
})

export class FieldLabel extends BulmaAnimatedComponent {
  get className () {
    return `field-label ${super.className}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </div>
    )
  }
}
export class FieldBody extends FieldLabel {
  get className () {
    return `field-body ${super.className}`
  }
}
