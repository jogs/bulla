import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

export default class InputCheck extends BulmaAnimatedComponent {
  get className () {
    return `checkbox ${super.className}`
  }
  render () {
    return (
      <label className={this.className} style={this.style}>
        <input {...this.nativeProps} type={'checkbox'} />
        {this.props.children}
      </label>
    )
  }
}
