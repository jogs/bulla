import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

export default class InputRadio extends BulmaAnimatedComponent {
  get className () {
    return `radio ${super.className}`
  }
  render () {
    return (
      <label className={this.className} style={this.style}>
        <input {...this.nativeProps} type={'radio'} />
        {this.props.children}
      </label>
    )
  }
}
