import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const tagClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  danger: 'is-danger',
  white: 'is-white',
  light: 'is-light',
  dark: 'is-dark',
  black: 'is-black',
  link: 'is-link',
  small: 'is-small',
  medium: 'is-medium',
  large: 'is-large'
}

export default class Tag extends BulmaAnimatedComponent {
  get tagClases () {
    return this.tagProps.filter(prop => this.props[prop])
    .map(prop => tagClases[prop])
    .join(' ')
  }
  get tagProps () {
    return Object.keys(tagClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.tagProps]
  }
  get className () {
    return `tag ${super.className} ${this.tagClases}`
  }
  render () {
    return (
      <span {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </span>
    )
  }
}

Tag.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  danger: false,
  white: false,
  light: false,
  dark: false,
  black: false,
  link: false,
  small: false,
  medium: false,
  large: false
})
