import BulmaAnimatedComponent from '../core/BulmaAnimatedComponent'
import React from 'react'

const notificationClases = {
  primary: 'is-primary',
  info: 'is-info',
  success: 'is-success',
  warning: 'is-warning',
  danger: 'is-danger'
}

export default class Notification extends BulmaAnimatedComponent {
  get notificationClases () {
    return this.notificationProps.filter(prop => this.props[prop])
    .map(prop => notificationClases[prop])
    .join(' ')
  }
  get notificationProps () {
    return Object.keys(notificationClases)
  }
  get extendedProps () {
    return [...super.extendedProps, ...this.notificationProps]
  }
  get className () {
    return `notification ${super.className} ${this.notificationClases}`
  }
  render () {
    return (
      <div {...this.nativeProps} className={this.className} style={this.style} >
        {this.props.children}
      </div>
    )
  }
}

Notification.defaultProps = Object.assign({}, BulmaAnimatedComponent.defaultProps, {
  primary: false,
  info: false,
  success: false,
  warning: false,
  danger: false
})
