import {Component} from 'react'
import {difference} from 'lodash'
import 'bulma/css/bulma.css'

const bulmaClases = {
  textCentered: 'has-text-centered',
  textLeft: 'has-text-left',
  textRight: 'has-text-right',
  clearfix: 'is-clearfix',
  pulledLeft: 'is-pulled-left',
  pulledRight: 'is-pulled-right',
  overlay: 'is-overlay',
  fullwidth: 'is-fullwidth',
  marginless: 'is-marginless',
  paddingless: 'is-paddingless',
  unselectable: 'is-unselectable',
  hidden: 'is-hidden'
}

export default class BulmaComponent extends Component {
  get _bulmaClases () {
    return bulmaClases
  }
  get bulmaClases () {
    return this.bulmaProps.filter(prop => this.props[prop])
    .map(prop => this._bulmaClases[prop])
    .join(' ')
  }
  get bulmaProps () {
    return Object.keys(this._bulmaClases)
  }
  get extendedProps () {
    return this.bulmaProps
  }
  get className () {
    return `${this.props.className} ${this.bulmaClases}`
  }
  get nativeProps () {
    return difference(Object.keys(this.props), [...this.extendedProps, 'children'])
    .reduce((objectPrev, currentProp) => {
      let tmp = {}
      tmp[currentProp] = this.props[currentProp]
      return Object.assign({}, objectPrev, tmp)
    }, {})
  }
}

BulmaComponent.defaultProps = {
  className: '',
  textCentered: false,
  textLeft: false,
  textRight: false,
  clearfix: false,
  pulledLeft: false,
  pulledRight: false,
  overlay: false,
  fullwidth: false,
  marginless: false,
  paddingless: false,
  unselectable: false,
  hidden: false
}
